//
//  Model.swift
//  cluster
//
//  Created by Tunde Adegoroye on 06/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import PromiseKit
import SwiftyJSON
import Alamofire

class API {
    
    
    // MARK: Behance API Implementation
    func getTopBehancePosts(fromPage: Int) -> Promise<JSON> {
        
        return Promise { success, fail in
            
            Alamofire.request(Behance().getTopUrl(withPage: fromPage)).validate().responseJSON { response in
                
                switch response.result {
                    
                case .success(let value):
                    let results = JSON(value)
                    success(results["projects"])
                    break
                    
                case .failure(let error):
                    fail(error)
                    break
                    
                }
            }
        }
    }
    
    func getRecentBehancePosts(fromPage: Int) -> Promise<JSON> {
        
        return Promise { success, fail in
            
            Alamofire.request(Behance().getRecentUrl(withPage: fromPage)).validate().responseJSON { response in
                
                switch response.result {
                    
                case .success(let value):
                    let results = JSON(value)
                    success(results["projects"])
                    break
                    
                case .failure(let error):
                    fail(error)
                    break
                    
                }
            }
            
        }
    }
    
    func appendBehancePosts(shouldClear: Bool, result: JSON, posts: inout [JSON]) -> Promise<[JSON]>{
    
        return Promise { success, fail in
            
            if shouldClear {
                posts = [JSON]()
            }
            
            for (key,value):(String, JSON) in result {
                
                // Format the date in "Zulu time" (UTC)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
                
                // Empty JSON object
                let post: JSON = [
                    
                    "title": value["name"].string,
                    "summary": "",
                    "fileName": value["covers"]["original"].string,
                    "url": value["url"].string,
                    "timePosted": dateFormatter.string(from: Date(timeIntervalSince1970: value["owners"][0]["created_on"].double!))
                    
                ]
                
                posts.append(post)
                
            }
            
            success(posts)
        }
    }

    // MARK: Pexels API Implementation
    func getTopPexelsPosts(fromPage: Int) -> Promise<JSON> {
    
        return Promise { success, fail in
            
            let headers: HTTPHeaders = [
                "Authorization": Pexels().getAccessToken(),
                "Content-Type": "application/json"
            ]
            
            Alamofire.request(Pexels().getTopUrl(withPage: fromPage), headers: headers).validate().responseJSON { response in
                
                switch response.result {
                    
                case .success(let value):
                    let results = JSON(value)
                    success(results["photos"])
                    break
                    
                case .failure(let error):
                    fail(error)
                    break
                    
                }
            }
            
        }
    }
    
    func getRecentPexelsPosts(fromPage: Int) -> Promise<JSON> {
        
        return Promise { success, fail in
            
            let headers: HTTPHeaders = [
                "Authorization": Pexels().getAccessToken(),
                "Content-Type": "application/json"
            ]
            
            Alamofire.request(Pexels().getRecentUrl(withPage: fromPage), headers: headers).validate().responseJSON { response in
                
                switch response.result {
                    
                case .success(let value):
                    let results = JSON(value)
                    success(results["photos"])
                    break
                    
                case .failure(let error):
                    fail(error)
                    break
                    
                }
            }
            
        }
    }
    
    func appendPexelsPosts(shouldClear: Bool, result: JSON, posts: inout [JSON]) -> Promise<[JSON]>{
        
        return Promise { success, fail in
            
            if shouldClear {
                posts = [JSON]()
            }
            
            for (key,value):(String, JSON) in result {
                
                // Empty JSON object
                let post: JSON = [
                    
                    "title": value["photographer"].string,
                    "summary": "",
                    "fileName": value["src"]["large"].string,
                    "url": value["url"].string,
                    "timePosted": ""
                    
                ]
                
                posts.append(post)
                
            }
            
            success(posts)
            
        }
    }
    
    // MARK: Flickr API Implementation
    func getTopFlickrPosts(fromPage: Int) -> Promise<JSON> {
        
        return Promise { success, fail in
        
            Alamofire.request(Flickr().getTopUrl(withPage: fromPage)).responseJSON { response in
                
                switch response.result {
                    
                case .success(let value):
                    let results = JSON(value)
                    print(results)
                    success(results["photos"]["photo"])
                    break
                    
                case .failure(let error):
                    fail(error)
                    break
                    
                }
            }
            
        }
    }
    
    func getRecentFlickrPosts(fromPage: Int) -> Promise<JSON> {
        
        return Promise { success, fail in
            
            Alamofire.request(Flickr().getRecentUrl(withPage: fromPage)).responseJSON { response in
                
                switch response.result {
                    
                case .success(let value):
                    let results = JSON(value)
                    success(results["photos"]["photo"])
                    break
                    
                case .failure(let error):
                    fail(error)
                    break
                    
                }
            }
            
        }
    }
    
    func appendFlickrPosts(shouldClear: Bool, result: JSON, posts: inout [JSON]) -> Promise<[JSON]> {
        
        return Promise { success, fail in
            
            print(result)
            
            if shouldClear {
                posts = [JSON]()
            }
            for (key,value):(String, JSON) in result {

                let title = value["title"].string!
                let farmId = value["farm"].int!
                let serverId = value["server"].string!
                let id = value["id"].string!
                let secret = value["secret"].string!
                let ownerId = value["owner"].string!
            
                let fileName = "https://farm\(farmId).staticflickr.com/\(serverId)/\(id)_\(secret)_c.jpg"
                let url = "https://www.flickr.com/photos/\(ownerId)/\(id)"
                // Empty JSON object
                let post: JSON = [
                    
                    "title": title,
                    "summary": "",
                    "fileName": fileName,
                    "url": url,
                    "timePosted": ""
                    
                ]
                
                print(post)
                
                posts.append(post)
                
            }
            
            success(posts)
            
        }
    }

    // MARK: Dribble API Implementation
    func getTopDribbblePosts(fromPage: Int) -> Promise<JSON>{
        
        return Promise { success, fail in
            
            Alamofire.request(Dribbble().getTopUrl(withPage: fromPage)).validate().responseJSON { response in
                
                switch response.result {
                    
                case .success(let value):
                    success(JSON(value))
                    break
                    
                case .failure(let error):
                    fail(error)
                    break
                    
                }
            }
        }
    }
    
    func getRecentDribbblePosts(fromPage: Int) -> Promise<JSON>{
        
        return Promise { success, fail in
            
            Alamofire.request(Dribbble().getRecentUrl(withPage: fromPage)).validate().responseJSON { response in
                
                switch response.result {
                    
                case .success(let value):
                    success(JSON(value))
                    break
                    
                case .failure(let error):
                    fail(error)
                    break
                    
                }
            }
        }
    }
    
    func appendDribbblePosts(shouldClear: Bool, result: JSON, posts: inout [JSON]) -> Promise<[JSON]>{
        
        return Promise { success, fail in
            
            if shouldClear {
                posts = [JSON]()
            }
            
            for (key,value):(String, JSON) in result {
                
                // Empty JSON object
                let post: JSON = [
                    
                    "title": value["title"].string,
                    "summary": value["description"].string,
                    "fileName": value["images"]["hidpi"].string != nil ? value["images"]["hidpi"].string : value["images"]["normal"].string,
                    "url": value["html_url"].string,
                    "timePosted": value["created_at"].string,
                    "isAnimated": value["animated"].bool
                    
                ]
                
                posts.append(post)
                
            }
            success(posts)
            
        }
    }
}
