//
//  APIViewController.swift
//  cluster
//
//  Created by Tunde Adegoroye on 06/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import PromiseKit

class APIViewController: UIViewController {

    internal var posts = [JSON]()
    internal var page = 1
    internal var api = API()
    
    
    @IBAction func dribbbleDidTouch(_ sender: Any) {
        
        api.getRecentBehancePosts(fromPage: 1).then { result -> Promise<[JSON]> in
            
            return self.api.appendBehancePosts(shouldClear: false, result: result, posts: &self.posts)
            
        }.always { posts -> Void in
            
            self.page += 1
            print("All done values from request:")
            print(self.posts)
            //print(self.posts[self.page])
            //print(self.posts[self.page - 1])
            print("Number of items: \(self.posts.count)")
            
        }
        
//        apiRequest(fromPage: page).then { result -> Promise<[JSON]> in
//            
//            return self.appendApi(shouldClear: false, result: result, posts: &self.posts)
//            
//        }.always { posts -> Void in
//            
//            
//            self.page += 1
//              print("All done values from request:")
//              print(self.posts)
//              //print(self.posts[self.page])
//              //print(self.posts[self.page - 1])
//              print("Number of items: \(self.posts.count)")
//        }
        

    }
    
    internal func apiRequest(fromPage: Int) -> Promise<JSON>{
        
        return Promise { success, fail in
            
            Alamofire.request(Flickr().getRecentUrl(withPage: fromPage)).responseJSON { response in
                
                switch response.result {
                    
                    case .success(let value):
                        let results = JSON(value)
                        success(results["photos"])
                    break
                        
                    case .failure(let error):
                        fail(error)
                    break
                    
                }
            }
        }
    }
    
    internal func appendApi(shouldClear: Bool, result: JSON, posts: inout [JSON]) -> Promise<[JSON]>{
        
        return Promise { success, fail in
            
            if shouldClear {
                posts = [JSON]()
            }
            
            for (key,value):(String, JSON) in result {
                
                // Format the date in "Zulu time" (UTC)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
                
                // Empty JSON object
                let post: JSON = [
                    
                    "title": "",
                    "summary": "",
                    "fileName": "https://farm\(value["farm"].string).staticflickr.com/\(value["server"].string)/\(value["id"].string)_\(value["secret"].string)_c.jpg",
                    "url": "https://www.flickr.com/photos/\(value["owner"].string)/\(value["id"].string)",
                    "timePosted": ""

                ]
                
                posts.append(post)
                
            }
            success(posts)
            
        }
    }

    /**
     
     Alamofire.request(Dribbble().getTopUrl(withPage: 1)).validate().responseJSON { response in
     
     switch response.result {
     
     case .success(let value):
     
     for (key,value):(String, JSON) in JSON(value) {
     
     // Empty JSON object
     let post: JSON = [
     
     "title": value["title"].string,
     "summary": value["description"].string,
     "fileName": value["images"]["hidpi"].string != nil ? value["images"]["hidpi"].string : value["images"]["normal"].string,
     "url": value["html_url"].string,
     "timePosted": value["created_at"].string
     
     ]
     
     self.posts.append(post)
     
     }
     
     print("Collection of posts: \(self.posts), Number of posts \(self.posts.count)")
     
     case .failure(let error):
     print(error)
     
     }
     }
     */
}
