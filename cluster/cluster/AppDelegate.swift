//
//  AppDelegate.swift
//  cluster
//
//  Created by Tunde Adegoroye on 04/04/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import iRate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    internal let colorScheme = Colors()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
        // Configure iRate
        iRate.sharedInstance().messageTitle = "Show us some ❤️"
        iRate.sharedInstance().message = "Are you enjoying all the slick inspiration we’re giving you? We’d appreciate your support if you took a moment to rate us 🤙🏾"
        
        // Tab bar text customization
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: colorScheme.supportingPink], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: colorScheme.lightGrey], for: .normal)
        
        // Tab bar background and border
        UITabBar.appearance().shadowImage = UIImage.generateImg(withColor: UIColor(red:0.94, green:0.95, blue:0.95, alpha:1.00))
        UITabBar.appearance().backgroundImage = UIImage.generateImg(withColor: .white)
        
            return true
    }

}

