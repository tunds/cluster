//
//  OptionsViewController.swift
//  cluster
//
//  Created by Tunde Adegoroye on 08/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import RealmSwift

protocol OptionsDelegate {
    func optionsDidFinishEditing()
}

class OptionsViewController: UIViewController, RemoveCollectionDelegate {

    internal var collection: Collection? = nil
    internal var delegate: OptionsDelegate!
    
    @IBOutlet weak var optionsContainerVw: UIView!
    @IBOutlet weak var optionsCollectionLbl: UILabel!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var removeBtn: UIButton!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Insert the gradient layer
        editBtn.layer.insertSublayer(Colors().getPrimaryGradient(bounds: editBtn.bounds), below: editBtn.imageView?.layer)
        removeBtn.layer.insertSublayer(Colors().getPrimaryGradient(bounds: removeBtn.bounds), below: removeBtn.imageView?.layer)
        
        // Assign the label
        optionsCollectionLbl.text = "Edit the collection"
        
        // Get the middle
        let middle = optionsContainerVw.frame.origin.y
        
        // Move to the top and fade it in
        optionsContainerVw.frame.origin = CGPoint(x: optionsContainerVw.frame.origin.x, y: -300)
        optionsContainerVw.isHidden = false
        
        // Drop the view down from the top
        UIView.animate(withDuration: 0.45, delay: 0.1, usingSpringWithDamping: 0.9, initialSpringVelocity: 6, options: .curveEaseInOut, animations: {
            
            self.optionsContainerVw.frame.origin = CGPoint(x: self.optionsContainerVw.frame.origin.x, y: middle)
            
        }, completion: nil)
    }

    @IBAction func editDidTouch(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "editVc") as! EditViewController
        vc.collection = collection
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func removeDidTouch(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "deleteVc") as! RemoveViewController
        vc.collection = collection
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func cancelDidTouch(_ sender: Any) {
        
        // Drop the view down out of the screen
        UIView.animate(withDuration: 0.7, delay: 0.1, usingSpringWithDamping: 0.9, initialSpringVelocity: 6, options: .curveEaseInOut, animations: {
            
            self.optionsContainerVw.frame.origin = CGPoint(x: self.optionsContainerVw.frame.origin.x, y: self.view.frame.height + self.optionsContainerVw.frame.height)
            
        }, completion: { finished in
            
            if finished {
                self.dismiss(animated: true, completion: nil)
            }
            
        })
    }
    
    // RemoveCollection delegate (When the user has finished removing the collection)
    func removeCollectionsDidFinish() {
        delegate.optionsDidFinishEditing()
        self.dismiss(animated: true, completion: nil)
    }
}
