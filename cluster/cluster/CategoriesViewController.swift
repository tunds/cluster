//
//  CategoriesViewController.swift
//  cluster
//
//  Created by Tunde Adegoroye on 08/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import DZNEmptyDataSet

protocol CategoriesTopViewControllerDelegate {
    func categoriesDidFinishEditing(topCell: TopPostCollectionViewCell)
}

protocol CategoriesRecentViewControllerDelegate {
    func categoriesDidFinishEditing(recentCell: RecentPostCollectionViewCell)
}

class CategoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CategoryTableViewCellDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, CreateCollectionDelegate {
    
    @IBOutlet weak var categoriesTblVw: UITableView!
    @IBOutlet weak var categoriesContainerVw: UIView!
    @IBOutlet weak var categoriesLbl: UILabel!
    
    internal let db = DB()
    internal var categories: Results<Collection>?
    internal var post: JSON?
    internal var topCell: TopPostCollectionViewCell?
    internal var recentCell: RecentPostCollectionViewCell?
    internal var topDelegate: CategoriesTopViewControllerDelegate?
    internal var recentDelegate: CategoriesRecentViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Assign the DZNEmptyDataSet source & delegate
        categoriesTblVw.emptyDataSetSource = self
        categoriesTblVw.emptyDataSetDelegate = self
        categoriesTblVw.tableFooterView = UIView()
        
        // Add the gradient
        categoriesLbl.textColor = UIColor.init(patternImage: UIImage(named: "Gradient")!)
        
        // Get the collections
        db.getCollections().then { results -> Void in
            self.categories = results
        }.always {
            self.categoriesTblVw.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Get the middle
        let middle = categoriesContainerVw.frame.origin.y
        
        categoriesContainerVw.isHidden = false

        // Move to the bottom and fade it in
        categoriesContainerVw.frame.origin = CGPoint(x: categoriesContainerVw.frame.origin.x, y:  self.view.frame.height + categoriesContainerVw.frame.height)
        
        // Drop the view down from the top
        UIView.animate(withDuration: 0.4, delay: 0.1, usingSpringWithDamping: 0.9, initialSpringVelocity: 6, options: .curveEaseInOut, animations: {
            
            self.categoriesContainerVw.frame.origin = CGPoint(x: self.categoriesContainerVw.frame.origin.x, y: middle)
            
        }, completion: nil)
        
    }

    @IBAction func closeDidTouch(_ sender: Any) {
        
        // Drop the view down from the top
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 6, options: .curveEaseInOut, animations: {
            
            // Move to the bottom and fade it out
            self.categoriesContainerVw.frame.origin = CGPoint(x: self.categoriesContainerVw.frame.origin.x, y:  self.view.frame.height + self.categoriesContainerVw.frame.height)
            
        }, completion: { finished in
            
            if finished {
                
                // Pass the cell depending on which view has navigated to this one
                if let topCell = self.topCell {
                    
                    self.topDelegate?.categoriesDidFinishEditing(topCell: topCell)
                }
                
                if let recentCell = self.recentCell {
                    self.recentDelegate?.categoriesDidFinishEditing(recentCell: recentCell)
                }
            
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    // MARK: CategoryTableViewCell delegate
    func categoryTableViewCellDidTouchToggle(cell: CategoryTableViewCell, collection: Collection, isActive: Bool) {
        
        if isActive {
            
            // Remove it
            db.removeFromCollection(collection: collection, withTitle: (post?["title"].string!)!)
            cell.setInActive()

        } else {
            
            // Add it
            db.addToCollection(collection: collection, post: post!)
            cell.setActive()
        }
    }

    // MARK: UITableView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if categories != nil {
            
            return categories!.count
            
        } else {
            
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell") as! CategoryTableViewCell
        cell.delegate = self
        cell.configureCategory(collection: (categories?[indexPath.row])!, post: post!)
        return cell
    }
    
    // MARK: CreateCollection delegate
    func collectionsCreatedDidFinish() {
        categoriesTblVw.reloadData()
    }
    
    // MARK: DZNEmptyDataSet delegate 
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "empty-box")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "It looks a bit empty here.."
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        paragraph.lineSpacing = 1.2
        
        let attributes = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 15), NSForegroundColorAttributeName: UIColor.lightGray, NSParagraphStyleAttributeName: paragraph]
        return NSAttributedString(string: text, attributes: attributes)
        
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString {
        let text = "It seems like you've not got any collections, no worries lets get that fixed 👇"
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.lineSpacing = 1.2
        paragraph.alignment = .center
        
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 12), NSForegroundColorAttributeName: UIColor.lightGray, NSParagraphStyleAttributeName: paragraph]
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        
        let text = "CREATE COLLECTION"
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        
        let attributes = [NSFontAttributeName: UIFont(name: ".SFUIText-Heavy", size: 16), NSForegroundColorAttributeName: UIColor.white, NSParagraphStyleAttributeName: paragraph]
        return NSAttributedString(string: text, attributes: attributes)
        
    }
    
    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        
        let capInsets = UIEdgeInsetsMake(25, 25, 25, 25)
        let rectInsets = UIEdgeInsetsMake(-3, -20, -3, -20)
        return UIImage(named: "button")!.resizableImage(withCapInsets: capInsets, resizingMode: .stretch).withAlignmentRectInsets(rectInsets)
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "createVc") as! CreateCollectionViewController
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }

}
