//
//  CategoryTableViewCell.swift
//  cluster
//
//  Created by Tunde Adegoroye on 08/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON

protocol CategoryTableViewCellDelegate {
    func categoryTableViewCellDidTouchToggle(cell: CategoryTableViewCell, collection: Collection, isActive: Bool)
}

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryNameLbl: UILabel!
    @IBOutlet weak var categoryBtn: UIButton!
    
    internal let db = DB()
    internal var delegate: CategoryTableViewCellDelegate?
    internal var collection: Collection?
    internal var post: JSON?
    internal var gradientSubLyr:CALayer?
    internal var isActive: Bool?
    
    /**
     Configure the category
     
     - parameter collection: The collection the post belongs to.
     
     - parameter post: The post to assign to the cell.
     
     */
    func configureCategory(collection: Collection, post: JSON) {
        
        // Assign
        gradientSubLyr = Colors().getPrimaryGradient(bounds: self.bounds)
        self.post = post
        self.collection = collection
        categoryNameLbl.text = collection.name
        
        // If the post is already in the collection set the button
        if db.existsInCollection(withUrl: post["url"].string!, collection: collection) {
            setActive()
        } else {
            setInActive()
        }
    }
    
    /**
     Set the button as active or not
     */
    func setActive(){
        
        isActive = true
        UIView.animate(withDuration: 0.2) {
            self.categoryBtn.backgroundColor = Colors().green
        }
        
    }
    
    /**
     Set the button as inactive or not
     */
    func setInActive(){
        
        isActive = false
        UIView.animate(withDuration: 0.2) {

            self.categoryBtn.backgroundColor = Colors().lightGrey
        }
    }
    
    @IBAction func categoryBtnDidTouch(_ sender: Any) {
        delegate?.categoryTableViewCellDidTouchToggle(cell: self, collection: collection!, isActive: isActive!)
    }
    
}
