//
//  CollectionViewController.swift
//  cluster
//
//  Created by Tunde Adegoroye on 05/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Hero
import RealmSwift
import SwiftyJSON

class CollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, HeroViewControllerDelegate, MyCollectionViewCellDelegate, RemovePostViewControllerDelegate, PostViewControllerDelegate {

    @IBOutlet weak var mySavedCollectionVw: UICollectionView!
    internal var posts: List<Post>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Apply styles
        UINavigationController.applyShadow(onNavigationController: self.navigationController!)
        self.navigationItem.title = "Inspiration"
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.init(patternImage: UIImage(named: "Gradient")!)]
    }
    
    // MARK: PostViewController delegate
    func postViewControllerDidFinishEditing() {
        mySavedCollectionVw.reloadData()
    }
    
    // MARK: RemovePostViewController delegate
    func removePostViewControllerDidFinishEditing(index: Int) {
        mySavedCollectionVw.reloadData()
    }
    
    // MARK: MyCollectionViewCell delegate
    func myCollectionViewCellDidTouchPostOptions(cell: MyCollectionViewCell) {
        
        // Pass the post to the delete vc
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "deletePostVc") as! RemovePostViewController
        vc.index = cell.currentIndexPath
        vc.post = cell.post
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }


    // MARK: Collectionview delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (posts?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mySavedCollectionCell", for: indexPath) as! MyCollectionViewCell
        cell.delegate = self
        cell.configurePostCell(indexPath: indexPath, post: (posts?[indexPath.row])!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // Create the JSON object to send to the post view
        let post = posts?[indexPath.row]
        
        let selectedPost: JSON = [
            
            "title": post?.title,
            "summary": post?.summary,
            "fileName": post?.fileName,
            "url": post?.url,
            "timePosted": ""
            
        ]
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "postVc") as! PostViewController
        vc.heroID = String(indexPath.row)
        vc.postInfo = selectedPost
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    // MARK: Hero delegate
    func heroWillStartAnimatingFrom(viewController: UIViewController) {

        // Set up the hero modifier
        mySavedCollectionVw.heroModifiers = [.cascade]
        
    }


}
