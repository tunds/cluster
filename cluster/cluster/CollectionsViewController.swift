//
//  CollectionsViewController.swift
//  cluster
//
//  Created by Tunde Adegoroye on 19/04/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import RealmSwift
import DZNEmptyDataSet

class CollectionsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, MyCollectionsViewCellDelegate, CreateCollectionDelegate, OptionsDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    @IBOutlet weak var userCollectionsVw: UICollectionView!
    
    internal let db = DB()
    internal var collections: Results<Collection>? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hook up the DZNEmptyDataSet delegate and datasource
        userCollectionsVw.emptyDataSetDelegate = self
        userCollectionsVw.emptyDataSetSource = self
        
        // Apply styles
        UINavigationController.applyShadow(onNavigationController: self.navigationController!)
       self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.init(patternImage: UIImage(named: "Gradient")!)]
        
        // The root view is used for the back button, so this is why it's set here
        UINavigationController.applyCustomBackBtn(onNavigationController: self.navigationController!, navigationItem: self.navigationItem)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)

        // Get all of the users collections
        db.getCollections().then { results -> Void in
            self.collections = results
        }.always {
            // Refresh the collectionview
            self.userCollectionsVw.reloadData()
        }
        
    }
    
    @IBAction func addDidTouch(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "createVc") as! CreateCollectionViewController
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    // MARK: OptionsViewController delegate (When the user has finished all the editing)
    func optionsDidFinishEditing() {
        
        userCollectionsVw.isUserInteractionEnabled = false
        
        // Get all of the users collections
        db.getCollections().then { results -> Void in
            self.collections = results
        }.always {
            // Refresh the collectionview
            self.userCollectionsVw.reloadData()
            self.userCollectionsVw.isUserInteractionEnabled = true
        }
    }
    
    // MARK: CreateCollection delegate (When the user has finished creating collection)
    func collectionsCreatedDidFinish() {
        
        userCollectionsVw.isUserInteractionEnabled = false

        // Get all of the users collections
        db.getCollections().then { results -> Void in
            self.collections = results
            }.always {
                // Refresh the collectionview
                self.userCollectionsVw.reloadData()
                self.userCollectionsVw.isUserInteractionEnabled = true

        }

    }
    
    // MARK: MyCollectionsViewCell delegate 
    func myCollectionsViewCellOptionsDidTouch(cell: MyCollectionsViewCell) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "optionsVc") as! OptionsViewController
        vc.collection = collections?[(cell.currentIndexPath?.row)!]
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }

    // MARK: Collectionviewcell delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let collections = collections {
            return collections.count
        } else {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCollectionsCell", for: indexPath) as! MyCollectionsViewCell
        cell.delegate = self
        cell.configureCollectionCell(collection: (collections?[indexPath.row])!, indexPath: indexPath)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        // Look into options popover
        if (collections?[indexPath.row].posts.count)! > 0 {
            
            performSegue(withIdentifier: "collectionSegue", sender: indexPath)
        
        }
    }
    
    // MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "collectionSegue" {
            
            // Pass on the posts to the collectionview controller
            let destinationVc = segue.destination as! CollectionViewController
            let indexPath = sender as! IndexPath
            destinationVc.posts = collections![indexPath.row].posts

        }
    }
    
    // MARK: DZNEmptyDataSet delegate
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "empty-box")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "It looks a bit empty here.."
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        paragraph.lineSpacing = 1.2
        
        let attributes = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 15), NSForegroundColorAttributeName: UIColor.lightGray, NSParagraphStyleAttributeName: paragraph]
        return NSAttributedString(string: text, attributes: attributes)
        
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString {
        let text = "It seems like you've not got any collections, no worries lets get that fixed 👇"
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.lineSpacing = 1.2
        paragraph.alignment = .center
        
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 12), NSForegroundColorAttributeName: UIColor.lightGray, NSParagraphStyleAttributeName: paragraph]
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        
        let text = "CREATE COLLECTION"
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        
        let attributes = [NSFontAttributeName: UIFont(name: ".SFUIText-Heavy", size: 16), NSForegroundColorAttributeName: UIColor.white, NSParagraphStyleAttributeName: paragraph]
        return NSAttributedString(string: text, attributes: attributes)
        
    }
    
    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        
        let capInsets = UIEdgeInsetsMake(25, 25, 25, 25)
        let rectInsets = UIEdgeInsetsMake(-3, -20, -3, -20)
        return UIImage(named: "button")!.resizableImage(withCapInsets: capInsets, resizingMode: .stretch).withAlignmentRectInsets(rectInsets)
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "createVc") as! CreateCollectionViewController
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }

}
