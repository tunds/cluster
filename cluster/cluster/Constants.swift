//
//  Constants.swift
//  cluster
//
//  Created by Tunde Adegoroye on 18/04/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//
import UIKit
import SwiftyJSON

// MARK: API's
struct Dribbble {
    
    static let access_token = "ff7849ad7973bf2e5de9cd02c3126360ac35629f405aba9d9696745ac8e9fb9c"
    static let base_url = "https://api.dribbble.com/v1/"
    
    func getTopUrl(withPage: Int) -> String {
        return "\(Dribbble.base_url)shots?access_token=\(Dribbble.access_token)&page=\(withPage)&per_page=6"
    }
    
    func getRecentUrl(withPage: Int) -> String {
        return "\(Dribbble.base_url)shots?access_token=\(Dribbble.access_token)&page=\(withPage)&per_page=6&list=debuts"
    }

}

struct Behance {
    
    static let access_token = "JiBxbkXMSTPGKuUpFgT9TCH4EdDAI6CU"
    static let base_url = "https://api.behance.net/v2/"
    
    func getTopUrl(withPage: Int) -> String {
        return "\(Behance.base_url)projects?client_id=\(Behance.access_token)&page=\(withPage)&per_page=6&sort=appreciations"
    }

    func getRecentUrl(withPage: Int) -> String {
        return "\(Behance.base_url)projects?client_id=\(Behance.access_token)&page=\(withPage)&per_page=6&sort=published_date"
    }
    
}

struct Pexels {
    
    static let access_token = "563492ad6f91700001000001484fc2f5074b482a7162f39a7fd75483"
    static let base_url = "https://api.pexels.com/v1/"
    
    func getTopUrl(withPage: Int) -> String {
        return "\(Pexels.base_url)popular?per_page=6&page=\(withPage)"
    }
    
    func getRecentUrl(withPage: Int) -> String {
        return "\(Pexels.base_url)search?query=inspiration&per_page=6&page=\(withPage)"
    }
    
    func getAccessToken() -> String {
        return Pexels.access_token
    }
}

struct Flickr {
    
    static let access_token = "3da92b6585208871b4559f9e3320289f"
    static let base_url = "https://api.flickr.com/services/rest/"
    
    func getTopUrl(withPage: Int) -> String {
        return "\(Flickr.base_url)?api_key=\(Flickr.access_token)&method=flickr.photos.getPopular&page=\(withPage)&per_page=6&format=json&nojsoncallback=1"
    }
    
    func getRecentUrl(withPage: Int) -> String {
        return "\(Flickr.base_url)?api_key=\(Flickr.access_token)&method=flickr.photos.getRecent&page=\(withPage)&per_page=6&format=json&nojsoncallback=1"
    }
}

// MARK: Color scheme
struct Colors {
    
    let darkGrey = UIColor(red:0.74, green:0.76, blue:0.78, alpha:1.00)
    let lightGrey = UIColor(red:0.87, green:0.87, blue:0.89, alpha:1.00)
    let supportingPink = UIColor(red:0.97, green:0.45, blue:0.45, alpha:1.00)
    let border = UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.00)
    let green = UIColor(red:0.21, green:0.90, blue:0.49, alpha:1.00)
    let red = UIColor(red:1.00, green:0.37, blue:0.33, alpha:1.00)
    
    func getPrimaryGradient(bounds: CGRect) -> CAGradientLayer {
        let gradientLyr = CAGradientLayer()
        gradientLyr.frame = bounds
        
        let start = UIColor(red:0.96, green:0.31, blue:0.64, alpha:1.00).cgColor as CGColor
        let end = UIColor(red:1.00, green:0.46, blue:0.47, alpha:1.00).cgColor as CGColor
        
        gradientLyr.colors = [start, end]
        
        gradientLyr.startPoint = CGPoint(x: 0, y: 0)
        gradientLyr.endPoint = CGPoint(x: 1, y: 1)
        
        return gradientLyr
    }
}

// MARK: Sources
struct Sources {
    
    enum Services: String {
        case dribbble = "Dribbble", behance = "Behance", pexels = "Pexels", flickr = "Flickr"
    }
    
    let services: [Services] = [.dribbble, .behance, .pexels, .flickr]
}

// MARK: Grid state
enum SourceState: String {
    case grid = "grid", panel = "panel", changing = "changing"
}

// MARK: Post selected states
enum PostStateControls: String {
    case visible = "visible", hidden = "hidden"
}



















