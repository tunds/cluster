//
//  CreateCollectionViewController.swift
//  cluster
//
//  Created by Tunde Adegoroye on 07/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit

protocol CreateCollectionDelegate {
    func collectionsCreatedDidFinish()
}

class CreateCollectionViewController: UIViewController {

    @IBOutlet weak var containerVw: UIView!
    @IBOutlet weak var collectionTxtField: UITextField!
    @IBOutlet weak var confirmBtn: UIButton!
    internal let db = DB()
    internal var delegate:CreateCollectionDelegate!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Insert the gradient into the button
        confirmBtn.layer.insertSublayer(Colors().getPrimaryGradient(bounds: confirmBtn.bounds), below: confirmBtn.imageView?.layer)
        
        // Get the middle
        let middle = containerVw.frame.origin.y
        
        // Move to the top and fade it in
        containerVw.frame.origin = CGPoint(x: containerVw.frame.origin.x, y: -300)
        containerVw.isHidden = false
        
        // Drop the view down from the top
        UIView.animate(withDuration: 0.45, delay: 0.1, usingSpringWithDamping: 0.9, initialSpringVelocity: 6, options: .curveEaseInOut, animations: {
           
            self.containerVw.frame.origin = CGPoint(x: self.containerVw.frame.origin.x, y: middle)
            
        }, completion: nil)
        
    }

    @IBAction func confirmDidTouch(_ sender: Any) {

        // If the text field is empty
        if (collectionTxtField.text?.isEmpty)! {
            
            collectionTxtField.error(collectionTxtField: collectionTxtField)
            collectionTxtField.shake(txtField: collectionTxtField)
            
        } else {
            
           // Check to see if the collection view with the name already exists
            if !db.collectionExists(withTitle: collectionTxtField.text!) {
                
                db.createCollection(withTitle: collectionTxtField.text!)
                collectionTxtField.text = ""
                
            } else {
                collectionTxtField.error(collectionTxtField: collectionTxtField)
                collectionTxtField.shake(txtField: collectionTxtField)
            }
        }
    }
    
    @IBAction func cancelDidTouch(_ sender: Any) {
        
        delegate.collectionsCreatedDidFinish()
        
        // Drop the view down out of the screen
        UIView.animate(withDuration: 0.7, delay: 0.1, usingSpringWithDamping: 0.9, initialSpringVelocity: 6, options: .curveEaseInOut, animations: {
            
            self.containerVw.frame.origin = CGPoint(x: self.containerVw.frame.origin.x, y: self.view.frame.height + self.containerVw.frame.height)
            
        }, completion: { finished in
            
            if finished {
                self.dismiss(animated: true, completion: nil)
            }
            
        })
        
    }
    
}
