//
//  EditViewController.swift
//  cluster
//
//  Created by Tunde Adegoroye on 08/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit

class EditViewController: UIViewController {

    @IBOutlet weak var optionsContainerVw: UIView!
    @IBOutlet weak var optionsCollectionLbl: UILabel!
    @IBOutlet weak var optionsTxtField: UITextField!
    @IBOutlet weak var confirmBtn: UIButton!
    
    internal var collection: Collection? = nil
    internal let db = DB()

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Insert the gradient layer
        confirmBtn.layer.insertSublayer(Colors().getPrimaryGradient(bounds: confirmBtn.bounds), below: confirmBtn.imageView?.layer)
        
        // Assign the label
        optionsCollectionLbl.text = "Edit name for \(collection!.name)"
        
        // Get the middle
        let middle = optionsContainerVw.frame.origin.y
        
        // Move to the top and fade it in
        optionsContainerVw.frame.origin = CGPoint(x: optionsContainerVw.frame.origin.x, y: -300)
        optionsContainerVw.isHidden = false
        
        // Drop the view down from the top
        UIView.animate(withDuration: 0.45, delay: 0.1, usingSpringWithDamping: 0.9, initialSpringVelocity: 6, options: .curveEaseInOut, animations: {
            
            self.optionsContainerVw.frame.origin = CGPoint(x: self.optionsContainerVw.frame.origin.x, y: middle)
            
        }, completion: nil)
    }

    @IBAction func confirmDidTouch(_ sender: Any) {
        
        // If the textfield isn't empty
        if !(optionsTxtField.text?.isEmpty)! {
            
            // Check to see if the collection view with the name already exists
            if !db.collectionExists(withTitle: optionsTxtField.text!) {
                
                db.updateCollection(withId: (collection?.id)!, withTitle: optionsTxtField.text!)
                
                // Drop the view down out of the screen
                UIView.animate(withDuration: 0.7, delay: 0.1, usingSpringWithDamping: 0.9, initialSpringVelocity: 6, options: .curveEaseInOut, animations: {
                    
                    self.optionsContainerVw.frame.origin = CGPoint(x: self.optionsContainerVw.frame.origin.x, y: self.view.frame.height + self.optionsContainerVw.frame.height)
                    
                }, completion: { finished in
                    
                    if finished {
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                })
                
            } else {
                
                optionsTxtField.error(collectionTxtField: optionsTxtField)
                optionsTxtField.shake(txtField: optionsTxtField)
            }
            
        } else {
            
            optionsTxtField.error(collectionTxtField: optionsTxtField)
            optionsTxtField.shake(txtField: optionsTxtField)
            
        }
        
    }
    
    /*
     // If the text field is empty
     if (collectionTxtField.text?.isEmpty)! {
     
     collectionTxtField.error(collectionTxtField: collectionTxtField)
     collectionTxtField.shake(txtField: collectionTxtField)
     
     } else {
     
     // Check to see if the collection view with the name already exists
     if !db.collectionExists(withTitle: collectionTxtField.text!) {
     
     db.createCollection(withTitle: collectionTxtField.text!)
     collectionTxtField.text = ""
     
     } else {
     collectionTxtField.error(collectionTxtField: collectionTxtField)
     collectionTxtField.shake(txtField: collectionTxtField)
     }
     }

     
     */
    
    @IBAction func cancelDidTouch(_ sender: Any) {
        
        // Drop the view down out of the screen
        UIView.animate(withDuration: 0.7, delay: 0.1, usingSpringWithDamping: 0.9, initialSpringVelocity: 6, options: .curveEaseInOut, animations: {
            
            self.optionsContainerVw.frame.origin = CGPoint(x: self.optionsContainerVw.frame.origin.x, y: self.view.frame.height + self.optionsContainerVw.frame.height)
            
        }, completion: { finished in
            
            if finished {
                self.dismiss(animated: true, completion: nil)
            }
            
        })
        
    }
    
}
