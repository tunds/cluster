//
//  Misc.swift
//  cluster
//
//  Created by Tunde Adegoroye on 19/04/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    func generateId() -> String {
        
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""
        
        for _ in 0..<7 {
            let randomValue = arc4random_uniform(UInt32(base.characters.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        
        return randomString
    }
    
}

extension UITextField {
    
    func shake(txtField: UITextField)  {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.1
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: txtField.center.x - 10, y: txtField.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: txtField.center.x + 10, y: txtField.center.y))
        txtField.layer.add(animation, forKey: "position")
        
    
    }
    
    func error(collectionTxtField: UITextField){
        collectionTxtField.layer.borderWidth = 1
        collectionTxtField.layer.masksToBounds = true
        collectionTxtField.layer.cornerRadius = 3
        collectionTxtField.layer.borderColor = Colors().red.cgColor
    }
    
}

extension UIButton {
    
    func applyRadius(withButton: UIButton, withRadius: CGFloat) {
        withButton.layer.cornerRadius = withRadius
    }
    
}

extension UIImageView {
    
    func rotateImgVw(withImgVw: UIImageView) {
        
        // Rotation animation
        let rotation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.fromValue = 0
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 0.8
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        withImgVw.layer.add(rotation, forKey: "rotationAnimation")
    }
}

extension UIImage {
    
    public class func generateImg(withColor: UIColor) -> UIImage {
        
        // Create a pixel
        let pixel = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        
        // Begin generating image
        UIGraphicsBeginImageContext(pixel.size)
        let context = UIGraphicsGetCurrentContext()
        
        // Fill the pixel
        context!.setFillColor(withColor.cgColor)
        context!.fill(pixel)
        
        // Get the context and return an image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
   public class func getTagLineImage(withCenter: CGPoint) -> UIImageView {
    
        // Create the image view and return the logo
        let imgVw = UIImageView(frame: CGRect(x: 0, y: 0, width: 72, height: 19))
        imgVw.contentMode = .scaleAspectFit
        imgVw.center = withCenter
        
        imgVw.image = UIImage(named: "brand-name")
        return imgVw
    }
    
}

extension UINavigationController {
    
   public class func applyShadow(onNavigationController: UINavigationController) {
      
        onNavigationController.navigationBar.setBackgroundImage(UIImage.generateImg(withColor: .white), for: .default)
        onNavigationController.navigationBar.shadowImage = UIImage()
        onNavigationController.navigationBar.layer.shadowColor = UIColor(red:0.93, green:0.94, blue:0.95, alpha:0.7).cgColor
        onNavigationController.navigationBar.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        onNavigationController.navigationBar.layer.shadowRadius = 4.0
        onNavigationController.navigationBar.layer.shadowOpacity = 1.0
        onNavigationController.navigationBar.layer.masksToBounds = false
        onNavigationController.navigationBar.layer.shouldRasterize = true
    }
    
    public class func applyCustomBackBtn(onNavigationController: UINavigationController, navigationItem: UINavigationItem){
        
        // Set the back button
        onNavigationController.navigationBar.backIndicatorImage = UIImage(named: "back")
        onNavigationController.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension UICollectionView {
    
     func setTopDefaultPanel(cell: TopSourceCollectionViewCell) -> UIView {
            
            // Configure the slider UI view
            let sliderVw = UIView()
            sliderVw.clipsToBounds = true
            sliderVw.layer.insertSublayer(Colors().getPrimaryGradient(bounds: cell.bounds), at: 0)
            sliderVw.layer.cornerRadius = 2
            
            // Calculate the x coordinate of the cell within the view
            let x = cell.frame.midX - (cell.frame.width / 2)
            
            // Calculate the space on the left hand side, this is the amount to push the cell to the left
            let offset = (cell.frame.width - cell.sourceLbl.intrinsicContentSize.width) / 2
            sliderVw.frame = CGRect(x: x + offset, y: cell.frame.height - 12, width: cell.sourceLbl.intrinsicContentSize.width, height: 4)
            
            return sliderVw
        
    }
    
    func setRecentDefaultPanel(cell: RecentSourceCollectionViewCell) -> UIView {
        
        // Configure the slider UI view
        let sliderVw = UIView()
        sliderVw.clipsToBounds = true
        sliderVw.layer.insertSublayer(Colors().getPrimaryGradient(bounds: cell.bounds), at: 0)
        sliderVw.layer.cornerRadius = 2
        
        // Calculate the x coordinate of the cell within the view
        let x = cell.frame.midX - (cell.frame.width / 2)
        
        // Calculate the space on the left hand side, this is the amount to push the cell to the left
        let offset = (cell.frame.width - cell.sourceLbl.intrinsicContentSize.width) / 2
        sliderVw.frame = CGRect(x: x + offset, y: cell.frame.height - 12, width: cell.sourceLbl.intrinsicContentSize.width, height: 4)
        
        return sliderVw
        
    }
}

extension UICollectionViewCell {
    
    func applyShadow() {
        
        // Shadow
        self.clipsToBounds = true
        self.layer.shadowColor = Colors().darkGrey.cgColor
        self.layer.shadowRadius = 10
        self.layer.shadowOpacity = 1
        self.layer.masksToBounds = false
        
        // Position and size of the shadow is done here via
        self.layer.shadowPath = UIBezierPath(roundedRect: CGRect(x: 15, y: 15, width: self.bounds.width - 30, height: self.bounds.height), cornerRadius: 0).cgPath
        
    }
    
}

extension TopSourceCollectionViewCell {
    
    
    func moveBottomPanel(cell: TopSourceCollectionViewCell, sliderVw: UIView) {

        UIView.animate(withDuration: 0.3, animations: {
            
            // Calculate the x coordinate of the cell within the view
            let x = cell.frame.midX - (cell.frame.width / 2)
            
            // Calculate the space on the left hand side, this is the amount to push the cell to the left
            let offset = (cell.frame.width - cell.sourceLbl.intrinsicContentSize.width) / 2
            
            sliderVw.frame = CGRect(x: x + offset, y: cell.frame.height - 12, width: cell.sourceLbl.intrinsicContentSize.width, height: 4)
            
        })
    }
}

extension RecentSourceCollectionViewCell {
    
    
    func moveBottomPanel(cell: RecentSourceCollectionViewCell, sliderVw: UIView) {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            // Calculate the x coordinate of the cell within the view
            let x = cell.frame.midX - (cell.frame.width / 2)
            
            // Calculate the space on the left hand side, this is the amount to push the cell to the left
            let offset = (cell.frame.width - cell.sourceLbl.intrinsicContentSize.width) / 2
            
            sliderVw.frame = CGRect(x: x + offset, y: cell.frame.height - 12, width: cell.sourceLbl.intrinsicContentSize.width, height: 4)
            
        })
    }
}

extension UIViewController {
    
    func setContentImg(state: SourceState, btn: UIBarButtonItem){
        
        switch state {
            case .grid:
                UIView.animate(withDuration: 0.6, animations: {
                    btn.image = UIImage(named: "panel")
                })
                break
            case .panel:
                UIView.animate(withDuration: 0.6, animations: {
                    btn.image = UIImage(named: "grid")
                })
                break
            default:
                break
        }
    }
    
    func addBottomLoadingIndicator(size: CGFloat, screenHeight: CGFloat, view: UIView) -> UIImageView {
    
        // Rotation animation
        let rotation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.fromValue = 0
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 0.8
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        
        // Loading indicator
        let loadingIndicatorView = UIImageView(image: UIImage(named: "loading-indicator"))
        loadingIndicatorView.tag = 100
        loadingIndicatorView.layer.cornerRadius = 3
        loadingIndicatorView.alpha = 0
        loadingIndicatorView.frame = CGRect(x: view.frame.midX - (size/2), y: screenHeight, width: size, height: size)
        loadingIndicatorView.layer.add(rotation, forKey: "rotationAnimation")
        
        return loadingIndicatorView
        
    }
    
    func addSideLoadingIndicator(size: CGFloat, scrollContentSizeWidth: CGFloat, scrollView: UIScrollView) -> UIImageView {
        
        // Rotation animation
        let rotation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.fromValue = 0
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 0.8
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        
        // Loading indicator
        let loadingIndicatorView = UIImageView(image: UIImage(named: "loading-indicator"))
        loadingIndicatorView.tag = 100
        loadingIndicatorView.layer.cornerRadius = 3
        loadingIndicatorView.alpha = 0
        loadingIndicatorView.frame = CGRect(x: scrollContentSizeWidth + 5, y: (scrollView.frame.height / 2) - (size/2), width: size, height: size)
        loadingIndicatorView.layer.add(rotation, forKey: "rotationAnimation")
        
        return loadingIndicatorView
        
    }
    
    func fadeInLoadingIndicator(loading: UIImageView){
    
        UIView.animate(withDuration: 0.3, animations: {
            loading.alpha = 1
        })
    }
 
    func removeLoadingIndicator(view: UIView, loading: UIImageView, completion: @escaping () -> ()){
        
        UIView.animate(withDuration: 0.3, animations: { 
            loading.alpha = 0
        }) { finished in
            
            if finished {
            
                if let indicatorVw = view.viewWithTag(100) {
                    indicatorVw.removeFromSuperview()
                    completion()
                }
            }
        }
    }
    
}
