//
//  Models.swift
//  cluster
//
//  Created by Tunde Adegoroye on 18/04/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import RealmSwift
import PromiseKit
import SwiftyJSON

// MARK: Configuration
class Config: Object {
    
    dynamic var isUpdatedInBg = false
    dynamic var isGifPlayable = true
}

// MARK: Class to hold posts
class Post: Object {
    
    dynamic var id = ""
    dynamic var title = ""
    dynamic var summary = ""
    dynamic var fileName = ""
    dynamic var url = ""
    dynamic var timePosted: Date?
    let collections = LinkingObjects(fromType: Collection.self, property: "posts")

    override static func primaryKey() -> String? {
        return "id"
    }
  
}

// MARK: Collections and their posts
class Collection: Object {
    
    dynamic var id = ""
    dynamic var name = ""
    var posts = List<Post>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}


class DB {
    
   internal let realm = try! Realm()
    
    // MARK: Writes
    func createCollection(withTitle: String) {
        
        // Creating a new collection
        let newCollection = Collection()
        newCollection.id = String().generateId()
        newCollection.name = withTitle
        
        try! realm.write {
            realm.add(newCollection, update: true)
        }
    }
    
    func addToCollection(collection: Collection, post: JSON) {
        
        // The current post
        let userPost = Post()
        userPost.id = String().generateId()
        userPost.fileName = post["fileName"].string!
        userPost.summary = post["summary"].string!
        userPost.title = post["title"].string!
        userPost.url = post["url"].string!
        
        // Creating a new collection
        try! realm.write {
            collection.posts.append(userPost)
        }
    }
    
    // MARK: Queries
    func existsInCollection(withUrl: String, collection: Collection) -> Bool {
        
        return collection.posts.filter(NSPredicate(format: "url = %@", withUrl)).count > 0 ? true : false
    }
    
    func postExists(withUrl: String) -> Bool {
        
        let predicate = NSPredicate(format: "url = %@", withUrl)
        
        return realm.objects(Post.self).filter(predicate).count > 0 ? true : false
        
    }
    
    func collectionExists(withTitle: String) -> Bool {
        
        let predicate = NSPredicate(format: "name = %@", withTitle)
        
        return realm.objects(Collection.self).filter(predicate).count > 0 ? true : false
        
    }
    
    func getCollections() -> Promise<Results<Collection>> {
        
        return Promise { success, fail in
            
             success(realm.objects(Collection.self))
        }
    }
    
    // MARK: Update
    func updateCollection(withId: String, withTitle: String) {
        
        // Creating a new collection
        let newCollection = Collection()
        newCollection.id = withId
        newCollection.name = withTitle
        
        try! realm.write {
            realm.add(newCollection, update: true)
        }
    }
    
    // MARK: Delete
    func removeFromCollection(collection: Collection, withTitle: String) {
        
        try! realm.write {
            realm.delete(collection.posts.filter(NSPredicate(format: "title = %@", withTitle)))
        }
    }
    
    func deleteCollection(withId: String) {
        
        // Creating a new collection
        let predicate = NSPredicate(format: "id = %@", withId)
        
        let collection = realm.objects(Collection.self).filter(predicate)
        
        try! realm.write {
            realm.delete(collection)
        }
    }

    func deletePost(withUrl: String) {
        
        // Creating a new collection
        let predicate = NSPredicate(format: "url = %@", withUrl)
        
        let collection = realm.objects(Post.self).filter(predicate)
        
        try! realm.write {
            realm.delete(collection)
        }
    }

}
