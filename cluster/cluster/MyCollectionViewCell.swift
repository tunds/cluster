//
//  MyCollectionViewCell.swift
//  cluster
//
//  Created by Tunde Adegoroye on 05/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Hero
import FLAnimatedImage

protocol MyCollectionViewCellDelegate {
    func myCollectionViewCellDidTouchPostOptions(cell: MyCollectionViewCell)
}

class MyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var postImgVw: FLAnimatedImageView!
    @IBOutlet weak var postTitleLbl: UILabel!
    @IBOutlet weak var postSummaryLbl: UILabel!
    @IBOutlet weak var postOptionsBtn: UIButton!
    
    internal var delegate: MyCollectionViewCellDelegate?
    internal var post: Post?
    internal var currentIndexPath:Int?
    
    @IBAction func postOptionsDidTouch(_ sender: Any) {
        delegate?.myCollectionViewCellDidTouchPostOptions(cell: self)
    }
    
    /**
     Configure the collectionviewcell
     
     - parameter indexPath: The indexpath of the cell.
     
     - parameter post: The post to assign to the cell.
     
     */
    func configurePostCell(indexPath: IndexPath, post: Post){
        
        // Hide all controls
        postOptionsBtn.alpha = 0
        postTitleLbl.alpha = 0
        postSummaryLbl.alpha = 0
        
        // Get the data
        let title = post.title
        let summary = post.summary
        self.post = post
        currentIndexPath = indexPath.row
        
        // Setup the hero config
        postImgVw.heroID = String(indexPath.row)
        self.heroModifiers = [.fade, .scale(0.2)]

        // Download the image asynchronously
        postImgVw.sd_setShowActivityIndicatorView(true)
        postImgVw.sd_setIndicatorStyle(.whiteLarge)
        postImgVw.sd_setImage(with: URL(string: post.fileName), placeholderImage: UIImage(named: "placeholder"),  options: [], completed: { (image, error, cache, url) in
            
            if error == nil {
                
                // Set the controls
                self.postTitleLbl.text = title.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
                self.postSummaryLbl.text = summary.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
                
                // Fade them in
                UIView.animate(withDuration: 0.3, animations: {
                    
                    self.postOptionsBtn.alpha = 1
                    self.postTitleLbl.alpha = 1
                    self.postSummaryLbl.alpha = 1
                    
                })
            }
            
        })
        
        // Add shadow to the cell
        self.applyShadow()
    }
    
}
