//
//  MyCollectionsViewCells.swift
//  cluster
//
//  Created by Tunde Adegoroye on 05/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import RealmSwift
import FLAnimatedImage

protocol MyCollectionsViewCellDelegate {
    func myCollectionsViewCellOptionsDidTouch(cell: MyCollectionsViewCell)
}

class MyCollectionsViewCell: UICollectionViewCell {

    @IBOutlet weak var collectionsNameLbl: UILabel!
    @IBOutlet weak var bigPanelImgVw: FLAnimatedImageView!
    @IBOutlet weak var smallPanel1ImgVw: FLAnimatedImageView!
    @IBOutlet weak var smallPanel2ImgVw: FLAnimatedImageView!
    @IBOutlet weak var noPostImgVw: UIImageView!
    
    var currentIndexPath: IndexPath?
    
    var delegate: MyCollectionsViewCellDelegate?
    
    /**
     Configure the collectionviewcell
     
     - parameter indexPath: The indexpath of the cell.
     
     - parameter collection: The collection which is holding the posts.
     
     */
    func configureCollectionCell(collection: Collection, indexPath: IndexPath){
        
        // Hide all the image views
        bigPanelImgVw.isHidden = true
        smallPanel1ImgVw.isHidden = true
        smallPanel2ImgVw.isHidden = true
        
        // If there are posts in a collection
        if collection.posts.count > 0 {
            
            // Show the images depending on the amount
            noPostImgVw.isHidden = true
           
            for index in 1...collection.posts.count {
                
                switch index - 1 {
                    
                    case 0:
                        setImage(imgVw: bigPanelImgVw, url: collection.posts[index-1].fileName)
                        bigPanelImgVw.isHidden = false
                        break
                    
                    case 1:
                        setImage(imgVw: smallPanel1ImgVw, url: collection.posts[index-1].fileName)
                        smallPanel1ImgVw.isHidden = false
                        break
                    
                    case 2:
                        setImage(imgVw: smallPanel2ImgVw, url: collection.posts[index-1].fileName)
                        smallPanel2ImgVw.isHidden = false
                        break

                    default:
                        break
                }
                
            }
            
        } else {
            
            noPostImgVw.isHidden = false
            
        }
        
        // Hook up the indexPath
        currentIndexPath = indexPath
        
        // Assign the label
        collectionsNameLbl.text = collection.name
        
        // Add shadow to the cell
        self.applyShadow()
    }
    
    /**
     Configure the collectionviewcell
     
     - parameter imgVw: The imageview which is used.
     
     - parameter url: The url which is used to download the image.
     
     */
    func setImage(imgVw: FLAnimatedImageView, url: String) {
        
        imgVw.sd_setShowActivityIndicatorView(true)
        imgVw.sd_setIndicatorStyle(.whiteLarge)
        imgVw.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeholder"),  options: [], completed: nil)
        
    }
    
    @IBAction func optionsDidTouch(_ sender: Any) {
        delegate?.myCollectionsViewCellOptionsDidTouch(cell: self)
    }
    
}
