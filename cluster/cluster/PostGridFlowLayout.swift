//
//  PostGridFlowLayout.swift
//  cluster
//
//  Created by Tunde Adegoroye on 01/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit

class PostGridFlowLayout: UICollectionViewFlowLayout {

    // Set up the flow collection view class
    override init() {
        super.init()
        configureLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureLayout()
    }
    
    // Set the item size property
    override var itemSize: CGSize {
        set {
            self.itemSize = CGSize(width: itemWidth(), height: itemHeight())
            
        }
        get {
            return CGSize(width: itemWidth(), height: itemHeight())
        }
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
    
    // MARK: Functions
    internal func configureLayout() {
        
        // Configure the spacing
        minimumLineSpacing = 30.0
        minimumInteritemSpacing = 30.0
        sectionInset = UIEdgeInsetsMake(30, 15, 30, 15)
    }
    
    internal func itemWidth() -> CGFloat {
        
        // Width is 50% of the view with 30 spacing
        return (collectionView!.frame.width * 0.5) - 30
    }
    
    internal func itemHeight() -> CGFloat {
        
        // Height is an extra 10px based on the width of the item
        return ((collectionView!.frame.width * 0.5) - 30) + 10
    }
}
