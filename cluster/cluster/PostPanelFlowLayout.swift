//
//  PostPanelFlowLayout.swift
//  cluster
//
//  Created by Tunde Adegoroye on 01/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit

class PostPanelFlowLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        configureLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureLayout()
    }
    
    // Set the item size property
    override var itemSize: CGSize {
        set {
            self.itemSize = CGSize(width: itemWidth(), height: itemHeight())
            
        }
        get {
            return CGSize(width: itemWidth(), height: itemHeight())
        }
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        return collectionView!.contentOffset
    }
    
    // MARK: Functions
    internal func configureLayout() {
        
        // Configure the spacing
        minimumLineSpacing = 17
        minimumInteritemSpacing = 15
        sectionInset = UIEdgeInsetsMake(27, 17, 25, 17)
        scrollDirection = .horizontal
    }
    
    internal func itemWidth() -> CGFloat {

        return (collectionView!.frame.width * 0.9) - 16
    }
    
    internal func itemHeight() -> CGFloat {
        
       return (collectionView!.frame.width * 0.8) + 80
    }
}

