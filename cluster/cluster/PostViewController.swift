//
//  PostViewController.swift
//  cluster
//
//  Created by Tunde Adegoroye on 04/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Hero
import SwiftyJSON
import FLAnimatedImage
import SafariServices
import Social

protocol PostViewControllerDelegate {
    func postViewControllerDidFinishEditing()
}

class PostViewController: UIViewController, UIScrollViewDelegate, SFSafariViewControllerDelegate {

    internal var postInfo:JSON!
    internal var heroID = ""
    internal var status: PostStateControls = .visible
    internal var panGr: UIPanGestureRecognizer!
    internal var delegate: PostViewControllerDelegate?
    
    @IBOutlet weak var postImgVw: FLAnimatedImageView!
    @IBOutlet weak var closeOptionsContentVw: UIView!
    @IBOutlet weak var optionsContentVw: UIView!
    @IBOutlet weak var linkBtn: UIButton!
    @IBOutlet weak var fbShareBtn: UIButton!
    @IBOutlet weak var twitterShareBtn: UIButton!
    @IBOutlet weak var addToBtn: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var postImgScrollVw: UIScrollView!
    @IBOutlet weak var postImgContainerScrollVw: UIView!
    
    // Change the colour of the status bar to white
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the up pan gesture recoginser
        panGr = UIPanGestureRecognizer(target: self, action: #selector(pan))
        view.addGestureRecognizer(panGr)
        
        // Configure and set up the scrollview delegate
        postImgScrollVw.maximumZoomScale = 5.0
        postImgScrollVw.minimumZoomScale = 1.0
        postImgScrollVw.delegate = self
        
        // Insert the gradient into the button
        linkBtn.layer.insertSublayer(Colors().getPrimaryGradient(bounds: linkBtn.bounds), below: linkBtn.imageView?.layer)
        
        // Set the hero ID to the one passed into the view controller
        postImgVw.heroID = heroID
        
        // Check to make sure the url isn't empty
        linkBtn.isUserInteractionEnabled = postInfo["url"].string != "" ? true : false
        
        // Set the image view
        postImgVw.sd_setShowActivityIndicatorView(true)
        postImgVw.sd_setIndicatorStyle(.whiteLarge)
        postImgVw.sd_setImage(with: URL(string: postInfo["fileName"].string!), placeholderImage: UIImage(named: "placeholder"),  options: [], completed: { (image, error, cache, url) in
        })
        
    }
    
    // UI Animations are in viewdidappear since the view has loaded
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Slide down the close button
        UIView.animate(withDuration: 0.45, delay: 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 4, options: .curveEaseInOut, animations: {
            self.closeOptionsContentVw.frame.origin = CGPoint(x: 0, y: 0)
        }, completion: nil)
        
        // Slide up the options view
        UIView.animate(withDuration: 0.45, delay: 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 4, options: .curveEaseInOut, animations: {
            self.optionsContentVw.frame.origin = CGPoint(x: 0, y: 488)
        }, completion: nil)
        
        // Slide up the link button
        UIView.animate(withDuration: 0.45, delay: 0.1, usingSpringWithDamping: 0.7, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.linkBtn.frame.origin = CGPoint(x: 20, y: 14)
        }, completion: nil)
        
        // Slide up the FB share button
        UIView.animate(withDuration: 0.45, delay: 0.2, usingSpringWithDamping: 0.7, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.fbShareBtn.frame.origin = CGPoint(x: 168, y: 13)
        }, completion: nil)
        
        // Slide up the Twitter share button
        UIView.animate(withDuration: 0.45, delay: 0.3, usingSpringWithDamping: 0.7, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.twitterShareBtn.frame.origin = CGPoint(x: 217, y: 13)
        }, completion: nil)
        
        // slide up the add button
        UIView.animate(withDuration: 0.45, delay: 0.4, usingSpringWithDamping: 0.7, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.addToBtn.frame.origin = CGPoint(x: 266, y: 13)
        }, completion: nil)
        
    }
    
    @IBAction func dismissDidTouch(_ sender: Any) {
        
        delegate?.postViewControllerDidFinishEditing()
        hero_dismissViewController()
    }
    
    @IBAction func linkBtnDidTouch(_ sender: Any) {
        let safariVC = SFSafariViewController(url: URL(string: postInfo["url"].string!)!, entersReaderIfAvailable: false)
        safariVC.delegate = self
        self.present(safariVC, animated: true, completion: nil)
        
    }
    
    @IBAction func facebookDidTouch(_ sender: Any) {
        
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook){
            
            let vc = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            vc?.add(URL(string: postInfo["url"].string!))
            self.present(vc!, animated: true, completion: nil)
            
        } else {
            
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func twitterDidTouch(_ sender: Any) {
        
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter){
     
            let vc = SLComposeViewController(forServiceType:SLServiceTypeTwitter)
            vc?.add(URL(string: postInfo["url"].string!))
            self.present(vc!, animated: true, completion: nil)
            
        } else {
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Twitter account to share.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func addToButtonDidTouch(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "categoriesVc") as! CategoriesViewController
        vc.post = postInfo
        self.present(vc, animated: true, completion: nil)
        
    }
    
    // MARK: Safari view controller delegate
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }

    
    // MARK: Pan Gesture Action
    func pan(){
        
        // Get the progress of the animation based on the screen touch
        let translation = panGr.translation(in: nil)
        let progress = translation.y / 2 / view.bounds.height
        
        // Based on the state of the pan gesture
        switch panGr.state {
            
            case .began:
                hero_dismissViewController()
                break
            
            case .changed:
                
                // Move the scrollview based on where the user has dragged on the screen
                Hero.shared.update(progress: Double(progress))
                let currentPos = CGPoint(x: translation.x + view.center.x, y: translation.y + view.center.y)
                Hero.shared.apply(modifiers: [.position(currentPos)], to: postImgVw)
                
                break
            
            default:
                
                // If the view goes out of bounds, end the animation or else send the view back to the center
                if progress + panGr.velocity(in: nil).y / view.bounds.height > 0.3 {
                    delegate?.postViewControllerDidFinishEditing()
                    Hero.shared.end()
                } else {
                    Hero.shared.cancel()
                }
                
                break
            
        }
    
    }

    
    // MARK: ScrollView DELEGATE
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return postImgContainerScrollVw
    }

    
    // MARK: Touches DELEGATE
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        // Depending on the user touching the screen, either show or hide the controls
        if status == .hidden {
            showControls()
            status = .visible
        } else if status == .visible {
            hideControls()
            status = .hidden
        }
        
    }
    
    // MARK: Functions
    internal func hideControls(){
        
        // Slide down the close button
        UIView.animate(withDuration: 0.45, delay: 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 4, options: .curveEaseInOut, animations: {
            self.closeOptionsContentVw.frame.origin = CGPoint(x: 0, y: -50)
        }, completion: nil)
        
        // Slide up the options view
        UIView.animate(withDuration: 0.45, delay: 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 4, options: .curveEaseInOut, animations: {
            self.optionsContentVw.frame.origin = CGPoint(x: 0, y: 550)
        }, completion: nil)
        
        
    }
    
    internal func showControls(){
        
        // Slide down the close button
        UIView.animate(withDuration: 0.45, delay: 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 4, options: .curveEaseInOut, animations: {
            self.closeOptionsContentVw.frame.origin = CGPoint(x: 0, y: 0)
        }, completion: nil)
        
        // Slide up the options view
        UIView.animate(withDuration: 0.45, delay: 0.05, usingSpringWithDamping: 0.8, initialSpringVelocity: 4, options: .curveEaseInOut, animations: {
            self.optionsContentVw.frame.origin = CGPoint(x: 0, y: 488)
        }, completion: nil)
        
        
    }
}
