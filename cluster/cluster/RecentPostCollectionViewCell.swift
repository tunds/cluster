//
//  RecentPostCollectionViewCell.swift
//  cluster
//
//  Created by Tunde Adegoroye on 05/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Hero
import SwiftyJSON
import SDWebImage
import FLAnimatedImage

protocol RecentPostCollectionViewCellDelegate {
    func recentPostCollectionViewCellDidTouchToggle(indexPath: IndexPath)
}

class RecentPostCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var isGifVw: UIView!
    @IBOutlet weak var postImgVw: UIImageView!
    @IBOutlet weak var contentVw: UIView!
    @IBOutlet weak var postTitleLbl: UILabel!
    @IBOutlet weak var postSummaryLbl: UILabel!
    @IBOutlet weak var postCollectionBtn: UIButton!
    
    // Aspect ratio constraint
    @IBOutlet weak var gridAspectRatioConstraint: NSLayoutConstraint!
    @IBOutlet weak var panelAspectRatioConstraint: NSLayoutConstraint!
    
    // Trailing constraint on the button
    @IBOutlet weak var postPanelCollectionBtnTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var postGridCollectionBtnTrailingConstraint: NSLayoutConstraint!
    
    // Height constraint on the button
    @IBOutlet weak var postGridCollectionBtnHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var postPanelCollectionBtnHeightConstraint: NSLayoutConstraint!
    
    // Width constraint on the button
    @IBOutlet weak var postPanelCollectionBtnWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var postGridCollectionBtnWidthConstraint: NSLayoutConstraint!
    
    // Top constraint on the post title label
    @IBOutlet weak var postGridCollectionLblTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var postPanelCollectionLblTopConstraint: NSLayoutConstraint!
    
    // Left constraint on the post title label
    @IBOutlet weak var postGridCollectionLblLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var postPanelCollectionLblLeftConstraint: NSLayoutConstraint!
    
    var currentIndexPath: IndexPath?
    internal var delegate: RecentPostCollectionViewCellDelegate?

    func configurePostCell(state: SourceState, indexPath: IndexPath, source: JSON){
        
        // Set the variables to the JSON
        let image = source["fileName"].string != nil ? source["fileName"].string : ""
        let title = source["title"].string != nil ? source["title"].string : ""
        let summary = source["summary"].string != nil ? source["summary"].string : ""
        
        if source["isAnimated"].bool == nil || source["isAnimated"].bool == false {
            isGifVw.alpha = 0
        } else {
            isGifVw.alpha = 1
        }
        
        // Hide all the controls by default
        postCollectionBtn.alpha = 0
        postTitleLbl.alpha = 0
        postSummaryLbl.alpha = 0
        
        // Set the indexpath
        currentIndexPath = indexPath
        
        // Set the hero
        postImgVw.heroID = String(indexPath.row)
        
        // Download the image asynchronously
        postImgVw.sd_setShowActivityIndicatorView(true)
        postImgVw.sd_setIndicatorStyle(.whiteLarge)
        postImgVw.sd_setImage(with: URL(string: image!), placeholderImage: UIImage(named: "placeholder"),  options: [], completed: { (image, error, cache, url) in
            
            if error == nil {
                
                // Set the controls
                self.postTitleLbl.text = title?.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
                self.postSummaryLbl.text = summary?.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
                
                // Fade them in
                UIView.animate(withDuration: 0.4, animations: {
                    
                    self.postCollectionBtn.alpha = 1
                    self.postTitleLbl.alpha = 1
                    self.postSummaryLbl.alpha = 1
                    
                })
            }
            
        })
        
        // Apply styles
        applyDynamicStyles(state: state)
        
        // Add shadow to the cell
        self.applyShadow()
    }
    
    @IBAction func postCollectionToggleDidTouch(_ sender: Any) {
        delegate?.recentPostCollectionViewCellDidTouchToggle(indexPath: currentIndexPath!)
    }
    
    /**
     Set the button as active
     */
    func setActive() {
        
        UIView.animate(withDuration: 0.15) {
            self.postCollectionBtn.backgroundColor = Colors().red
            self.postCollectionBtn.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/4))
        }
    }
    
    /**
     Set the button as inactive
     */
    func setInActive() {
        
        UIView.animate(withDuration: 0.15) {
            self.postCollectionBtn.backgroundColor = Colors().green
            self.postCollectionBtn.transform = CGAffineTransform(rotationAngle: CGFloat(0))
        }
        
    }
    
    /**
     Apply the styles based on the state the user pressed
     
     - parameter state: The state of the cell.
     
     */
    func applyDynamicStyles(state: SourceState) {
        
        // Set the image ratio
        if state == .grid {
            
            // Change the trailing space of the button
            postGridCollectionBtnTrailingConstraint.isActive = true
            postPanelCollectionBtnTrailingConstraint.isActive = false
            
            // Toggle the image aspect ratio
            panelAspectRatioConstraint.isActive = false
            gridAspectRatioConstraint.isActive = true
            
            // Toggle the top space for the title label
            postGridCollectionLblTopConstraint.isActive = true
            postPanelCollectionLblTopConstraint.isActive = false
            
            // Toggle the left space for the title label
            postGridCollectionLblLeftConstraint.isActive = true
            postPanelCollectionLblLeftConstraint.isActive = false
            
            // Change the width & height
            postGridCollectionBtnHeightConstraint.isActive = true
            postGridCollectionBtnWidthConstraint.isActive = true
            
            postPanelCollectionBtnHeightConstraint.isActive = false
            postPanelCollectionBtnWidthConstraint.isActive = false
            
            // Style the font and size for the post title
            postTitleLbl.font = UIFont(name: postTitleLbl.font.fontName, size: 14)
            
            // Style the font and size for the post description
            postSummaryLbl.font = UIFont(name: postSummaryLbl.font.fontName, size: 8)
            
            // Add icon to the button
            postCollectionBtn.setImage(UIImage(named: "add"), for: .normal)
            
            // Add circle effect to the button
            postCollectionBtn.applyRadius(withButton: postCollectionBtn, withRadius: 13)
            
        } else if state == .panel {
            
            postGridCollectionBtnTrailingConstraint.isActive = false
            postPanelCollectionBtnTrailingConstraint.isActive = true
            
            gridAspectRatioConstraint.isActive = false
            panelAspectRatioConstraint.isActive = true
            
            postGridCollectionLblTopConstraint.isActive = false
            postPanelCollectionLblTopConstraint.isActive = true
            
            postGridCollectionLblLeftConstraint.isActive = false
            postPanelCollectionLblLeftConstraint.isActive = true
            
            postGridCollectionBtnHeightConstraint.isActive = false
            postGridCollectionBtnWidthConstraint.isActive = false
            
            postPanelCollectionBtnHeightConstraint.isActive = true
            postPanelCollectionBtnWidthConstraint.isActive = true
            
            postTitleLbl.font = UIFont(name: postTitleLbl.font.fontName, size: 18)
            
            postSummaryLbl.font = UIFont(name: postSummaryLbl.font.fontName, size: 10)
            
            postCollectionBtn.setImage(UIImage(named: "larger-add"), for: .normal)
            
            postCollectionBtn.applyRadius(withButton: postCollectionBtn, withRadius: 17)
        }
        
    }

}
