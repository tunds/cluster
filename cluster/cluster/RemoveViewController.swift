//
//  RemoveViewController.swift
//  cluster
//
//  Created by Tunde Adegoroye on 08/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit

protocol RemoveCollectionDelegate {
    func removeCollectionsDidFinish()
}

class RemoveViewController: UIViewController {

    internal var collection: Collection? = nil
    internal let db = DB()
    internal var delegate:RemoveCollectionDelegate!

    @IBOutlet weak var optionsContainerVw: UIView!
    @IBOutlet weak var removeBtn: UIButton!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Insert the gradient into the button
        removeBtn.layer.insertSublayer(Colors().getPrimaryGradient(bounds: removeBtn.bounds), below: removeBtn.imageView?.layer)
        
        // Get the middle
        let middle = optionsContainerVw.frame.origin.y
        
        // Move to the top and fade it in
        optionsContainerVw.frame.origin = CGPoint(x: optionsContainerVw.frame.origin.x, y: -300)
        optionsContainerVw.isHidden = false
        
        // Drop the view down from the top
        UIView.animate(withDuration: 0.45, delay: 0.1, usingSpringWithDamping: 0.9, initialSpringVelocity: 6, options: .curveEaseInOut, animations: {
            
            self.optionsContainerVw.frame.origin = CGPoint(x: self.optionsContainerVw.frame.origin.x, y: middle)
            
        }, completion: nil)
    }
    
    @IBAction func removeDidTouch(_ sender: Any) {
        
        self.dismiss(animated: true) {
            self.db.deleteCollection(withId: (self.collection?.id)!)
            self.delegate.removeCollectionsDidFinish()
        }
        
    }
    
    @IBAction func cancelDidTouch(_ sender: Any) {
        
        // Drop the view down out of the screen
        UIView.animate(withDuration: 0.7, delay: 0.1, usingSpringWithDamping: 0.9, initialSpringVelocity: 6, options: .curveEaseInOut, animations: {
            
            self.optionsContainerVw.frame.origin = CGPoint(x: self.optionsContainerVw.frame.origin.x, y: self.view.frame.height + self.optionsContainerVw.frame.height)
            
        }, completion: { finished in
            
            if finished {
                self.dismiss(animated: true, completion: nil)
            }
            
        })
    
    }
    
}
