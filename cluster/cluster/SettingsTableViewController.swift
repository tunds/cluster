//
//  SettingsTableViewController.swift
//  cluster
//
//  Created by Tunde Adegoroye on 05/05/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import MessageUI

class SettingsTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var updateSwitch: UISwitch!
    @IBOutlet weak var gifSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Fix to remove the extra cells at the bottom of the tableview
        tableView.tableFooterView = UIView()
        tableView.separatorColor = UIColor(red:0.78, green:0.78, blue:0.80, alpha:0.3)
    }
    
    // MARK: Mail delegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    // MARK: UITableview Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
            case 0:
                
                if MFMailComposeViewController.canSendMail() {
                    
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setToRecipients(["tundsdev@yahoo.co.uk"])
                    mail.setSubject("Cluster - Feedback or possible feature request")
                    present(mail, animated: true, completion: nil)
                    
                }
                
                break
            case 1:
                
                UIApplication.shared.open(URL(string: "itms-apps://itunes.apple.com/app/id1215759359")!, options: [:], completionHandler: nil)
                
                break
            case 2:
                performSegue(withIdentifier: "acknowledgementsSegue", sender: nil)
                break
            default:
                break
        }
        
    }
}
