//
//  SettingsViewController.swift
//  cluster
//
//  Created by Tunde Adegoroye on 19/04/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBOutlet weak var brandImgVw: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Apply styles
        UINavigationController.applyShadow(onNavigationController: self.navigationController!)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.init(patternImage: UIImage(named: "Gradient")!)]
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Slide down the uiimageview
        UIView.animate(withDuration: 0.45, delay: 0.1, usingSpringWithDamping: 0.7, initialSpringVelocity:5, options: .curveEaseInOut, animations: {
            self.brandImgVw.frame.origin = CGPoint(x: self.view.center.x - (self.brandImgVw.frame.width / 2), y: 60)
        }, completion: nil)
    }

    @IBAction func followDidTouch(_ sender: Any) {
        
        UIApplication.shared.open(URL(string: "https://twitter.com/tundsdev")!, options: [:], completionHandler: nil)
    }
    
}
