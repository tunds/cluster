//
//  SourceCollectionViewCell.swift
//  cluster
//
//  Created by Tunde Adegoroye on 19/04/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit

class TopSourceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var sourceLbl: UILabel!
    
    var service: Sources.Services?
    
    /**
     Configure the source view cell
     
     - parameter withService: The service which will be attached to the cell.
     
     - parameter activeService: The service which is currently active.
     
     */
    func configureSourceCell(withService: Sources.Services, activeService: Sources.Services) {
        
        // Assign the service
        service = withService
        sourceLbl.text = service?.rawValue
         
        // Only when the service matches the active service
        if service == activeService {
        
            // Configure styles
            UIView.animate(withDuration: 0.5, animations: {
                self.sourceLbl.textColor = UIColor.init(patternImage: UIImage(named: "Gradient")!)

            })

        }

    }
    
    /**
     Reset the view cell
     */
    func resetSourceCell() {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.sourceLbl.textColor = Colors().darkGrey
            
        })
        
    }
    
}
