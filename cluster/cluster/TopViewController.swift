//
//  FirstViewController.swift
//  cluster
//
//  Created by Tunde Adegoroye on 04/04/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import PromiseKit
import DZNEmptyDataSet

class TopViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, TopPostCollectionViewCellDelegate, CategoriesTopViewControllerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    @IBOutlet weak var sourcesCollectionVw: UICollectionView!
    @IBOutlet weak var postCollectionVw: UICollectionView!
    @IBOutlet weak var contentStateItemBtn: UIBarButtonItem!
    @IBOutlet weak var loadingContainerVw: UIView!
    @IBOutlet weak var loadingContainerImgVw: UIImageView!
    @IBOutlet weak var loadingContainerLbl: UILabel!

    internal var page = 1
    internal var api = API()
    internal var posts = [JSON]()
    internal var loadingIndicator: UIImageView?
    internal let services = Sources().services
    internal let gridLayout = PostGridFlowLayout()
    internal let panelLayout = PostPanelFlowLayout()
    internal var activeService: Sources.Services = .dribbble
    internal var count = 6
    internal var sliderVw: UIView?
    internal var isFirstTime = true
    internal var contentState: SourceState = .grid
    internal let db = DB()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up the DZNEmptyDataSet delegate and dataset
        postCollectionVw.emptyDataSetSource = self
        postCollectionVw.emptyDataSetDelegate = self
        
        // Set up the loading indicator & loading view
        loadingContainerLbl.textColor = UIColor.init(patternImage: UIImage(named: "Gradient")!)
        loadingContainerVw.alpha = 1
        loadingContainerImgVw.alpha = 1
        loadingContainerImgVw.rotateImgVw(withImgVw: loadingContainerImgVw)
        
        // Get the default dribbble sources
        api.getTopDribbblePosts(fromPage: page).then { result -> Promise<[JSON]> in
            
            return self.api.appendDribbblePosts(shouldClear: true, result: result, posts: &self.posts)
            
        }.always { posts -> Void in
            
            self.postCollectionVw.reloadData()
            self.loadingContainerVw.alpha = 0
            self.loadingContainerImgVw.alpha = 0
        }
        

        // Set the flow layout for the sources collectionview
        sourcesCollectionVw.collectionViewLayout = SourcesFlowLayout()
        
        // Set the default layout for the collectionview
        postCollectionVw.collectionViewLayout = gridLayout
        
        // Apply styles
        navigationItem.titleView = UIImage.getTagLineImage(withCenter: self.view.center)
        UINavigationController.applyShadow(onNavigationController: self.navigationController!)
        
    }
    
    @IBAction func contentStateDidTouch(_ sender: Any) {
        
        // Tell the collection view layout changes will be made
        postCollectionVw.collectionViewLayout.invalidateLayout()
        
        if contentState == .grid {
            
            // Make the app aware the layout is changing
            contentState = .changing
            
            // Change the icon
            setContentImg(state: contentState, btn: contentStateItemBtn)
            
            // Change the layout
            postCollectionVw.setCollectionViewLayout(panelLayout, animated: false, completion: { finished in
                
                if finished {
                    
                    self.contentState = .panel
                    self.postCollectionVw.reloadData()
                    
                }
            })
            
        } else if contentState == .panel {
            
            // Make the app aware the layout is changing
            contentState = .changing
            
            setContentImg(state: contentState, btn: contentStateItemBtn)
            
            postCollectionVw.setCollectionViewLayout(gridLayout, animated: false, completion: { finished in
                
                if finished {
                    
                    self.contentState = .grid
                    self.postCollectionVw.reloadData()
                    
                }
            })
        }
    }
    
    // MARK: UIScrollview Delegates
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        // Only trigger on the post collectionview
        if scrollView == postCollectionVw {
            
            switch contentState {
                
            case .grid:
                
                // Get the height of the scrollview, get the height of the whole scrollview, get the current y position of the scrollview
                let scrollViewHeight = scrollView.frame.size.height;
                let scrollContentSizeHeight = scrollView.contentSize.height;
                let scrollOffset = scrollView.contentOffset.y;
                let size: CGFloat = 30
                
                // Check to see if we're at end which is the combination of the current y position and the addition of the height of the scrollview matches the height of the whole scrollview
                if (scrollOffset + scrollViewHeight == scrollContentSizeHeight) {
                    
                    // Let the application know that the content is changing, this is to prevent the UI from messing up
                    contentState = .changing
                    
                    // Disable selection change & scrolling
                    scrollView.isScrollEnabled = false
                    contentStateItemBtn.isEnabled = false
                    
                    // Move the scrollview upwards and turn off bouncing temporarily
                    scrollView.setContentOffset(CGPoint(x:0, y: scrollOffset + 60 ), animated: true)
                    scrollView.bounces = false
                    
                    // Get the loading indicator & add it to the view
                    loadingIndicator = self.addBottomLoadingIndicator(size: size, screenHeight: scrollContentSizeHeight, view: self.view)
                    scrollView.insertSubview(loadingIndicator!, aboveSubview: scrollView)
                    
                    // Fade it in
                    self.fadeInLoadingIndicator(loading: loadingIndicator!)
                    
                    // Append to the sources with new service
                    self.page += 1
                    getPosts(shouldRefresh: false, forSource: activeService, completion: {
                        
                        self.removeLoadingIndicator(view: self.view, loading: self.loadingIndicator!, completion: {
                            
                            // Reset all the controls and the tableview
                            self.contentState = .grid
                            self.postCollectionVw.reloadData()
                            scrollView.isScrollEnabled = true
                            self.contentStateItemBtn.isEnabled = true
                            self.postCollectionVw.isScrollEnabled = true
                            self.postCollectionVw.isUserInteractionEnabled = true
                            scrollView.bounces = true
                        })
                        
                    })

                }
                
                break
                
            case .panel:
                
                // Get the height of the scrollview, get the height of the whole scrollview, get the current y position of the scrollview
                let scrollViewWidth = scrollView.frame.size.width;
                let scrollContentSizeWidth = scrollView.contentSize.width;
                let scrollOffset = scrollView.contentOffset.x;
                let size: CGFloat = 30
                
                // Check to see if we're at end which is the combination of the current y position and the addition of the height of the scrollview matches the height of the whole scrollview
                if (scrollOffset + scrollViewWidth == scrollContentSizeWidth) {
                    
                    // Let the application know that the content is changing, this is to prevent the UI from messing up
                    contentState = .changing
                    
                    // Disable selection change & scrolling
                    scrollView.isScrollEnabled = false
                    contentStateItemBtn.isEnabled = false
                    
                    // Get the loading indicator & add it to the view
                    loadingIndicator = self.addSideLoadingIndicator(size: size, scrollContentSizeWidth: scrollContentSizeWidth, scrollView: scrollView)
                    scrollView.insertSubview(loadingIndicator!, aboveSubview: scrollView)
                    
                    // Move the scrollview upwards and turn off bouncing temporarily
                    scrollView.setContentOffset(CGPoint(x: scrollOffset + 70, y: 0 ), animated: true)
                    scrollView.bounces = false
                    
                    // Fade it in
                    self.fadeInLoadingIndicator(loading: loadingIndicator!)
                    
                    // Append to the sources with new service
                    self.page += 1
                    getPosts(shouldRefresh: false, forSource: activeService, completion: {
                        
                        self.removeLoadingIndicator(view: self.view, loading: self.loadingIndicator!, completion: {
                            
                            // Reset all the controls and the tableview
                            self.contentState = .panel
                            self.postCollectionVw.reloadData()
                            scrollView.isScrollEnabled = true
                            self.contentStateItemBtn.isEnabled = true
                            self.postCollectionVw.isScrollEnabled = true
                            self.postCollectionVw.isUserInteractionEnabled = true
                            scrollView.bounces = true
                        })
                        
                    })
                    
                }
                
                break
            default:
                break
                
            }
            
        }
        
    }
    
    // MARK: CategoriesViewController delegates
    func categoriesDidFinishEditing(topCell: TopPostCollectionViewCell) {

        // When the categories have finished being selecing, get the indexpath
        let indexPath = topCell.currentIndexPath
        
        // Use the indexPath of the cell, to see if it exists in a collection
        // If it does, then toggle the status of the button
        if db.postExists(withUrl: posts[(indexPath?.row)!]["url"].string!) {
            topCell.setActive()
        } else {
            topCell.setInActive()
        }
    }
    
    // MARK: TopPostCollectionViewCell delegates
    func topPostCollectionViewCellDidTouchToggle(indexPath: IndexPath) {
        
        // Go to the categories view
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "categoriesVc") as! CategoriesViewController
        vc.topDelegate = self
        vc.topCell = postCollectionVw.cellForItem(at: indexPath) as? TopPostCollectionViewCell
        vc.post = posts[indexPath.row]
        self.present(vc, animated: true, completion: nil)
        
    }
    
    // MARK: UICollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == sourcesCollectionVw {
            return services.count
        } else {
            
            return posts.count > 0 ? posts.count : 0

        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == sourcesCollectionVw {
        
            // For the first cell
            if collectionView.indexPathsForVisibleItems.count > 0 && isFirstTime {
                
                // Add default start position for the first cell and change the first time
                sliderVw = collectionView.setTopDefaultPanel(cell: collectionView.cellForItem(at: IndexPath(item: 0, section: 0)) as! TopSourceCollectionViewCell)
                collectionView.insertSubview(sliderVw!, aboveSubview: collectionView)
                isFirstTime = false
            }
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sourceCell", for: indexPath) as! TopSourceCollectionViewCell
            cell.configureSourceCell(withService: services[indexPath.row], activeService: activeService)
            return cell
            
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "postCell", for: indexPath) as! TopPostCollectionViewCell
            cell.delegate = self
            cell.configurePostCell(state: contentState, indexPath: indexPath, source: posts[indexPath.row])
            
            // Apply button toggle state
            if db.postExists(withUrl: posts[indexPath.row]["url"].string!) {
                cell.setActive()
            } else {
                cell.setInActive()

            }
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        // Only when the content isn't changing, then continue to refresh the posts
        if collectionView == sourcesCollectionVw && contentState != .changing {
            
            // Fade in the loading icon
            UIView.animate(withDuration: 0.4, animations: {
                
                self.loadingContainerVw.alpha = 1
                self.loadingContainerImgVw.alpha = 1
                
            })
            
            // Set it to a new service
            activeService = services[indexPath.row]
            
            // Refresh the sources with new service
            self.page = 1
            getPosts(shouldRefresh: true, forSource: activeService, completion: {

                self.postCollectionVw.reloadData()
                self.postCollectionVw.isScrollEnabled = true
                self.contentStateItemBtn.isEnabled = true
                self.postCollectionVw.isScrollEnabled = true
                self.postCollectionVw.isUserInteractionEnabled = true
                self.postCollectionVw.bounces = true
                
                UIView.animate(withDuration: 0.4, animations: {
                    
                    self.loadingContainerVw.alpha = 0
                    self.loadingContainerImgVw.alpha = 0
                    
                })
                
            })
            
            // Reset all of the cells
            for i in 0..<services.count {
                
                let cell = collectionView.cellForItem(at: IndexPath(row: i, section: 0)) as! TopSourceCollectionViewCell
                cell.resetSourceCell()
                
            }
            
            // Apply style onto the active cell
            let cell = collectionView.cellForItem(at: indexPath) as! TopSourceCollectionViewCell
            cell.configureSourceCell(withService: services[indexPath.row], activeService: activeService)
            
            // Go to the source and move the bottom panel
            collectionView.scrollToItem(at: indexPath, at: .right, animated: true)
            cell.moveBottomPanel(cell: cell, sliderVw: sliderVw!)
            
            
        } else if collectionView == postCollectionVw && contentState != .changing {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "postVc") as! PostViewController
            vc.heroID = String(indexPath.row)
            vc.postInfo = posts[indexPath.row]
            self.present(vc, animated: true, completion: nil)
            
        }
        
    }
    
    // MARK: Functions
    
    /**
     Get's posts depending on the source passed into the function
     
     - parameter shouldRefresh: Should the source be refreshed.
     
     - parameter forSource: The source which should be retrieved.
     
     - parameter completion: The completion handler when the data has been retrieved.

     */
    internal func getPosts(shouldRefresh: Bool, forSource: Sources.Services, completion: @escaping () -> ()){
        
        switch forSource {
            
            case .behance:
                
                api.getTopBehancePosts(fromPage: page).then { result -> Promise<[JSON]> in
                    
                    return self.api.appendBehancePosts(shouldClear: shouldRefresh, result: result, posts: &self.posts)
                    
                }.always { posts -> Void in
                        
                    completion()
                        
                }
                
                break
            
            case .dribbble:
                
                api.getTopDribbblePosts(fromPage: page).then { result -> Promise<[JSON]> in
                 
                    return self.api.appendDribbblePosts(shouldClear: shouldRefresh, result: result, posts: &self.posts)
                 
                 }.always { posts -> Void in
                    
                    completion()
                    
                 }
                
                break
            
            case .flickr:
                
                api.getRecentFlickrPosts(fromPage: page).then { result -> Promise<[JSON]> in
                    
                    return self.api.appendFlickrPosts(shouldClear: shouldRefresh, result: result, posts: &self.posts)
                    
                }.always { posts -> Void in
                        
                    completion()
                }
                
                break
            
            case .pexels:
                
                api.getTopPexelsPosts(fromPage: page).then { result -> Promise<[JSON]> in
                    
                    return self.api.appendPexelsPosts(shouldClear: shouldRefresh, result: result, posts: &self.posts)
                    
                }.always { posts -> Void in
                        
                    completion()
                        
                }
                break
        
        }
        
    }
    
    // MARK: DZNEmptyDataSet delegate
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "wifi")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "Yikes"
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        paragraph.lineSpacing = 1.2
        
        let attributes = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 15), NSForegroundColorAttributeName: UIColor.lightGray, NSParagraphStyleAttributeName: paragraph]
        return NSAttributedString(string: text, attributes: attributes)
        
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString {
        let text = "It seems like there's an issue gathering posts, please check you're internet connection."
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineBreakMode = .byWordWrapping
        paragraph.lineSpacing = 1.2
        paragraph.alignment = .center
        
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 12), NSForegroundColorAttributeName: UIColor.lightGray, NSParagraphStyleAttributeName: paragraph]
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        
        let text = "TRY AGAIN"
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        
        let attributes = [NSFontAttributeName: UIFont(name: ".SFUIText-Heavy", size: 16), NSForegroundColorAttributeName: UIColor.white, NSParagraphStyleAttributeName: paragraph]
        return NSAttributedString(string: text, attributes: attributes)
        
    }
    
    func buttonBackgroundImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        
        let capInsets = UIEdgeInsetsMake(25, 25, 25, 25)
        let rectInsets = UIEdgeInsetsMake(-3, -20, -3, -20)
        return UIImage(named: "button")!.resizableImage(withCapInsets: capInsets, resizingMode: .stretch).withAlignmentRectInsets(rectInsets)
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
      
        getPosts(shouldRefresh: true, forSource: activeService, completion: {
            
            self.postCollectionVw.reloadData()
            
        })
        
    }

    
}

